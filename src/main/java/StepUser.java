import model.NewURM;
import model.UserRequestModel;

public class StepUser {
    ApiUser apiUser = new ApiUser();

    public void createUser(String name, String job)
    {
        UserRequestModel model = new UserRequestModel(name, job);
        apiUser.createUser(model);
    }

    public void createUser(String name, String job, int id, String createdAt)
    {
        NewURM model = new NewURM(name, job, id, createdAt);
        apiUser.createUser(model);
    }
}
