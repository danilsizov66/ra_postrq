package model;

import java.text.DateFormat;
import java.util.Date;

public class NewURM
{
    public NewURM getURM()
    {
        return this;
    }
    public String getName() {
        return name;
    }
    public String getJob() {
        return job;
    }
    public int getId() {
        return id;
    }
    public String getCreatedAt() {
        return createdAt;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setJob(String job) {
        this.job = job;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    private String name;
    private String job;
    private int id;
    private String createdAt;

    public NewURM(String name, String job, int id, String createdAt) {
        setName(name);
        setId(id);
        setCreatedAt(createdAt);
        setJob(job);
    }
    public NewURM() {

    }
}
