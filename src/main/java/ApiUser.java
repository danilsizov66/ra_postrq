import groovy.transform.ToString;
import io.restassured.http.ContentType;
import model.NewURM;
import model.UserRequestModel;

import static io.restassured.RestAssured.given;

public class ApiUser {
    public void createUser(UserRequestModel model)
    {
        given()
                .baseUri("https://reqres.in/")
                .contentType(ContentType.JSON)
                .body(model)
                .post("api/users")
                .then()
                .statusCode(201)
                .log().all();
    }

    public NewURM createUser(NewURM model)
    {
        given()
                .baseUri("https://reqres.in/")
                .contentType(ContentType.JSON)
                .body(model)
                .post("api/users")
                .then()
                .statusCode(201)
                .log().all()
                .extract().response().as(NewURM.class);
        return model;
    }


    public NewURM getUser(NewURM model, int id)
    {
        given()
                .baseUri("https://reqres.in/")
                .contentType(ContentType.JSON)
                .body(model)
                .get("api/users/" + Integer.toString(id))
                .then()
                .statusCode(200)
                .log().all()
                .extract().response().as(NewURM.class);
        return model;
    }

}
