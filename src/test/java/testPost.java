
import model.NewURM;
import org.testng.Assert;
import org.testng.annotations.Test;

public class testPost {
    StepUser stepUser = new StepUser();
    NewURM modele = new NewURM("name", "job", 0, "date");

    @Test
    public void firstTest() {

        stepUser.createUser("Sizoveus", "leader", 12, "2022-11-02T13:10:56.002Z");
        Assert.assertEquals(stepUser.getClass().getName(), stepUser.apiUser.getUser(modele, 12).getName());
    }
}
